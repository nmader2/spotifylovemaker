import cherrypy
import pandas as pd
import re, json
import numpy as np
from spotify_library import _spotify_database

class SpotifyController(object):

  def __init__(self, db=None):
    if db is None:
      self.db1 = _spotify_database()
      self.db2 = _spotify_database()
    #else:
    #  self.db = db
    #self.db.load_data(r'Data.json')

  def GET_RATING(self, name1_name2):
    output = {'result' : 'success'}

    # run calculations from database data to get rating
    try:
      name1_name2 = str(name1_name2)
      names = name1_name2.split('_', 1)
      name1 = names[0]
      name2 = names[1]
      self.db1.load_data("data1.csv")
      # set default rating in case autoencoder fails
      rating = 61
      rating = str(rating)
      output['rating'] = rating
      try:
        import pytorch
        p_ds = [val for db1 in db1.data.to_dict().values]
        d_ds = [val for db2 in db2.data.to_dict().values]
        autoenc = Autoencoder()
        bat_size = 10
        max_epochs = 10
        train(autoenc, p_ds, bat_size, max_epochs)

        p_freq = make_freq_mat(autoenc, p_ds)
        d_freq = make_freq_mat(autoenc, d_ds)
        p_freq+=1.0e-5
        d_freq+=1.0e-5
        
        sum = 0.0
        for j in range(4):
          kl = sps.entropy(p_freq[j], d_freq[j])
          sum+=1
        rating = sum / 4
      except Exception as ex:
        p_freq = .5
        d_freq = .5
        
    except Exception as ex:
	    output['result'] = 'error'
	    output['message'] = str(ex)
    
    return json.dumps(output)


  def POST_DATA(self, name):
    output = {'result' : 'success'}

    try:
      data_string = cherrypy.request.body.read().decode('utf-8')
      output['message'] = data_string
      data = json.loads(data_string)
      print('data', data)
      if(name == 'name1'):
        self.db1.equals(pd.DataFrame.from_dict(data))
        self.db1.save_data("data1.csv")
      elif(name == 'name2'):
        self.db2.equals(pd.DataFrame.from_dict(data))
        self.db2.save_data("data2.csv")
      print("data frame", self.db1.data)

      #self.db.append({'Name':data['name'], 'ID':data['id']}) 
    except Exception as ex:
      output['result'] = 'failure'
      output['message'] = str(ex)
      #output['name'] = name
      #output['data'] = data
    
    return json.dumps(output)
