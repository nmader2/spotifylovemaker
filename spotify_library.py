import pandas as pd

class _spotify_database:

  def __init__(self):
    self.data = pd.DataFrame()

  def equals(self, dataframe):
    self.data = dataframe

  def load_data(self, data_file):
    self.data = pd.read_csv(data_file)

  def save_data(self, data_file):
    self.data.to_csv(data_file)
