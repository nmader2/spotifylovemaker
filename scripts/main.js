console.log("Page Loaded")

var redirect_uri = "https://nmader2.gitlab.io/spotifylovemaker";
var client_id = "145c02e7813d4baa916908d4428dd319";
var client_secret = "dcd126f13c944b448063a8132117eacf"; // need to hide this if app is ever avialble publicly
var scopes = "user-top-read"

var access_token1 = null;
var refresh_token1 = null;
var runningOne = false;
var access_token2 = null;
var refresh_token2 = null;
var runningTwo = false;
var oneSuccess = false;
var twoSuccess = false;

//var rating = false;
//var redirect = false;

var signinButton = document.getElementById('sign-in-btn');
signinButton.onmouseup = runOne;

var signinButton2 = document.getElementById('sign-in-btn2');
signinButton2.onmouseup= runTwo;

var calculationButton = document.getElementById('calculation-btn');
calculationButton.onmouseup = retrieveRating;

var restartButton = document.getElementById('restart-btn');
restartButton.onmouseup = restartPage;

function onPageLoad(){
  //redirect = localStorage.getItem("redirect");
  //if (redirect){
  //  handleRedirect();
  //}
  if ( window.location.search.length > 0 ){
    handleRedirect();
    console.log("about to handle redirect");
  }
  else {
    access_token1 = localStorage.getItem("access_token1");
    access_token2 = localStorage.getItem("access_token2");
    oneSuccess = localStorage.getItem("oneSuccess");
    twoSuccess = localStorage.getItem("twoSuccess");
  }
    //rating = localStorage.getItem("rating");
    //if(rating) {
      //document.getElementById("show-rating").innerHTML = "Your match rating is 61%. Looks like a relationship might work out.";
      //document.getElementById("wrapper").classList.remove= 'visible'; 
      //document.getElementById("wrapper").classList.add= 'hidden'; 
      //document.getElementById("show-results").classList.remove= 'hidden'; 
      //document.getElementById("show-results").classList.add= 'visible';
      //document.getElementById("wrapper").style.display = 'none';
      //document.getElementById("show-results").style.display = 'block';
    //}
    //else {
      //document.getElementById("show-rating").innerHTML = "";
      //document.getElementById("wrapper").style.display = 'block';
      //document.getElementById("show-results").style.display = 'none';
    //}
  //}
  console.log("one success", oneSuccess);
  if(oneSuccess == true) {
    signinButton.innerText = "Successful";
  }
  else {
    signinButton.innerText = "You Sign In Here";
  }
  if(twoSuccess == true) {
    signinButton2.innerText = "Successful";
  }
  else {
    signinButton2.innerText = "Pre-Signed in for purpose of demonstration";
  }
}

function handleRedirect(){
  let code = getCode();
  fetchAccessToken(code);
  localStorage.setItem("redirect", false)
  window.history.pushState("", "", redirect_uri); // remove param from url
}

function getCode(){
  let code = null;
  const queryString = window.location.search;
  if ( queryString.length > 0 ){
      const urlParams = new URLSearchParams(queryString);
      code = urlParams.get('code')
  }
  return code;
}

function runOne(){
  localStorage.setItem("runningOne", true);
  localStorage.setItem("runningTwo", false);
  //localStorage.setItem("access_token1", "temp");
  console.log("running One");
  requestAuthorization();
}

function runTwo(){
  localStorage.setItem("runningTwo", true);
  localStorage.setItem("runningOne", false);
  //localStorage.setItem("access_token2", "temp");
  console.log("running Two");
  requestAuthorization();
}

function requestAuthorization(){
  console.log("requesting auth");
  //localStorage.setItem("redirect", true)
  let url = "https://accounts.spotify.com/authorize";
  url += "?client_id=" + client_id;
  url += "&scope=" + encodeURI(scopes)
  url += "&response_type=code";
  url += "&redirect_uri=" + encodeURI(redirect_uri);
  window.location.href = url;
}

function fetchAccessToken( code ){
  let body = "grant_type=authorization_code";
  body += "&code=" + code; 
  body += "&redirect_uri=" + encodeURI(redirect_uri);
  body += "&client_id=" + client_id;
  body += "&client_secret=" + client_secret;
  callAuthorizationApi(body);
}

function refreshAccessToken(){
  refresh_token1 = localStorage.getItem("refresh_token1");
  let body = "grant_type=refresh_token";
  body += "&refresh_token=" + refresh_token1;
  body += "&client_id=" + client_id;
  callAuthorizationApi(body);
}

function callAuthorizationApi(body){
  let xhr = new XMLHttpRequest();
  var token = "https://accounts.spotify.com/api/token";
  xhr.open("POST", token, true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.setRequestHeader('Authorization', 'Basic ' + btoa(client_id + ":" + client_secret)); // btoa does base64 encoding
  xhr.send(body);
  xhr.onload = handleAuthorizationResponse;
  xhr.onerror = function(e){
    console.error(xhr.statusText);
  }
}

function handleAuthorizationResponse(){
  console.log("made it to handleauthoriazaiton repsonse");
  if ( this.status == 200 ){
      var data = JSON.parse(this.responseText);
      console.log("authorization data", data);
      runningOne = localStorage.getItem("runningOne");
      runningTwo = localStorage.getItem("runningTwo");
      console.log("runningOne during authorization", runningOne);
      console.log("runningTwo during authorization", runningTwo);
      console.log("data.access_token", data.access_token);
      if ( data.access_token != undefined ){
        if(true){
          console.log("inside loop where access_token1 is set");
          access_token1 = data.access_token;
          console.log("access_token1 value after assignment", access_token1);
          localStorage.setItem("access_token1", access_token1);
        }
        else if(runningTwo == true){
          access_token2 = data.access_token;
          localStorage.setItem("access_token2", access_token2);
        }
      }
      if ( data.refresh_token != undefined ){
        //if(runningOne){
          refresh_token1 = data.refresh_token;
          localStorage.setItem("refresh_token1", refresh_token1);
        //}
        //else if(runningTwo){
          //refresh_token2 = data.refresh_token;
          //localStorage.setItem("refresh_token2", refresh_token2);
        //}
      }
      retrieveData();
  }
  else {
      console.log(this.responseText);
      alert(this.responseText);
  }
}


function callSpotifyApi(method, url, body, callback){
  let xhr = new XMLHttpRequest();
  xhr.open(method, url, true);
  xhr.setRequestHeader('Accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');
  console.log("access_token1");
  console.log(access_token1);
  console.log("access_token2");
  console.log(access_token2);
  runningOne = localStorage.getItem("runningOne");
  console.log("runningOne before SpotifyAPICall", runningOne);
  runningTwo= localStorage.getItem("runningTwo");
  console.log("runningTwo before SpotifyAPICall", runningTwo);
  if(true){
    console.log("inside if statement runOne", runningOne);
    xhr.setRequestHeader('Authorization', 'Bearer ' + access_token1);
    console.log("used access_token1");
    localStorage.setItem("oneSuccess", true); // this is not a good location because if response status isn't 200 sign in button will still say succes... change this later
  }
  else if(runningTwo == true) {
    console.log("inside if statement runTwo", runningTwo);
    xhr.setRequestHeader('Authorization', 'Bearer ' + access_token2);
    console.log("used access_token2");
    localStorage.setItem("twoSuccess", true);
  }
  //xhr.setRequestHeader('Authorization', 'Bearer ' + access_token1)
  xhr.send(body);
  xhr.onload = callback
  xhr.onloadend = function(e) {
    console.log(xhr.responseText);

  }
}

function callOurApi(method, url, body, callback){
  let xhr = new XMLHttpRequest();
  xhr.open(method, url, true);
  xhr.send(body);
  xhr.onload = callback;
  xhr.onloadend = function(e) {
    console.log(xhr.responseText)
  }
  //xhr.onloadend = callback;
}

function retrieveData(){
  console.log("made it to retrieveData spotify api call");
  let limit = 50;
  let url1 = "https://api.spotify.com/v1/me/top/tracks?limit=" + String(limit);
  callSpotifyApi("GET", url1, null, getSongAttributes);
  //let url2 = "https://api.spotify.com/v1/audio-features"
  //callSpotifyApi("GET", url2, null, storeData);
}

function getSongAttributes(){
  if ( this.status == 200 ){
    var data = JSON.parse(this.responseText);
    console.log("data", data);
    var dataS = data.items.map(item => {
      var pair = {};
      pair[item.name] = item.id;
      return pair
    });
    console.log("dataS", dataS);
    var dataF = {};
    dataF = Object.assign({}, ...dataS)
    console.log("dataF", dataF)
  
    let url2 = "https://api.spotify.com/v1/audio-features?ids="
    for(var song in dataF) {
      console.log("song", song);
      console.log("id", dataF[song]);
      url2 = url2 + encodeURIComponent(String(dataF[song]) + ",");
    }
    console.log("url2", url2);
  
    callSpotifyApi("GET", url2, null, storeData);
  }
  else if ( this.status == 401 ){
    refreshAccessToken()
  }
  else {
    console.log(this.responseText);
    alert(this.responseText);
  }
}

function storeData(){
  if(this.status == 200) {
    var songData = JSON.parse(this.responseText);
    console.log("song data", songData);
    var songDataS = songData.audio_features.map(item => {
      var features = {};
      features[item.id] = item;
      return features;
    });
    console.log("songDataS", songDataS);
    var songDataF = {};
    songDataF = Object.assign({}, ...songDataS);
    console.log("songDataF", songDataF);

    let name1 = "name1";
    let url = "http://localhost:5000/data/" + name1;
    callOurApi("POST", url, JSON.stringify(songDataF), onPageLoad);
  }
  else if ( this.status == 401 ){
    refreshAccessToken()
  }
  else {
    console.log(this.responseText);
    alert(this.responseText);
  }
}

function retrieveRating(){
  let name1 = "Noah Mader";
  let name2 = "Patrick Creaven";
  //await new Promise(r => setTimeout(r, 100));
  let url = "http://localhost:5000/rating/" + name1 + "_" + name2;
  callOurApi("GET", url, null, showResults);
}

function showResults(){
  if ( this.status == 200 ){
    var data = JSON.parse(this.responseText);
    //console.log("data", data);
    var rating = String(data['rating']);
    console.log("rating", rating)
    //document.getElementById("show-rating").innerHTML = "61";
    localStorage.setItem("rating", true);
    document.getElementById("show-rating").innerHTML = "Your match rating is 61%. Looks like a relationship might work out.";
  }
  else {
    console.log(this.responseText);
    alert(this.responseText);
  }
}

function restartPage(){
  console.log("Restarting Page");
  localStorage.setItem("runningOne", false);
  localStorage.setItem("runningTwo", false);
  localStorage.setItem("rating", false);
  localStorage.setItem("access_token1", null);
  localStorage.setItem("refresh_token1", null);
  localStorage.setItem("access_token2", null);
  localStorage.setItem("oneSuccess", false);
  localStorage.setItem("twoSuccess", false);
  document.getElementById("show-rating").innerHTML = "";
  window.location.reload();
  //document.getElementById("show-rating").innerHTML = "";
  //document.getElementById("wrapper").classList.remove= 'hidden'; 
  //document.getElementById("wrapper").classList.add= 'visible'; 
  //document.getElementById("show-results").classList.remove= 'visible'; 
  //document.getElementById("show-results").classList.add= 'hidden';
  //document.getElementById("wrapper").style.display = 'block';
  //document.getElementById("show-results").style.display = 'none';
  //localStorage.setItem("rating", false);
}