# SpotifyLoveMaker

Spotify Love Maker Web Application

nmader2 & pcreaven

OO API:

Request Type Resource endpoint Body Expected response Inner working of handler GET /rating/name1_name2 There should be no body to a GET request the match percentage for the two names GET_MATCH calculates value using algorithm POST /data/name The body will be a json of all the data taken from the Spotify API. It will be the persons top 50 tracks with data about the genre and audio features of the app. {“response” : “success} POST_DATA stores all the data associated with the name in a database which is just a csv file

JSON Specification for the server:

The port number is 5000.
The way this webservice should be used is between two people who listen to music on Spotify, with their own Spotify accounts. Each person should sign into their accounts with the respective buttons on the webservice (ideally this sign-in is done on the same machine). Once they have signed in, our webservice then grabs their playlist information and runs the respective music taste datasets through a machine learning model that will ultimately calculate a percentage of similarity in their music taste. 

User Interaction:

Instructions on how to run the code:
1) To start the server you will run python3 server.py 
2) Go to the appropriate URL given later on in the READme
3) After this, on the website you will be prompted to sign in with your Spotify details, and then your person of interest will do the same on the application
4) After this you will then click the calculate button and a percentage from 0.0 to 100.0 will be given, with 100.0% being an exact match, and a 0.0% being a total opposite taste in music
***5) In the case of the Demo we had to use pre-determined data for user 2 due to time constraints in figuring out an issue with Spotify caching user 1's information after sign-in (i.e. it does not allow user two the opportunity to input their data as it automatically logs in user 1)

Tests:

To run our tests run python3 test_api.py and then run python3 test_ws.py
