import unittest
import numpy as np
from spotify_library import _spotify_database
import pandas as pd

class TestAPI(unittest.TestCase):

  def test_equals(self):
    db1 = _spotify_database()
    df = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),columns=['a', 'b', 'c'])
    db1.equals(df)
    self.assertEquals(db1.data.iloc[0]['a'], 1)

  def test_save_and_load_data(self):
    db1 = _spotify_database()
    df = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),columns=['a', 'b', 'c'])
    db1.equals(df)
    db1.save_data('Data.json')
    db1.load_data('Data.json')
    self.assertEquals(db1.data.iloc[1]['a'], 4)

if (__name__ == "__main__"):
  unittest.main()