import unittest
import requests
import json

class TestAPI(unittest.TestCase):

  SERVER_URL = 'http://localhost:5000/'
  GET_RATING_URL = SERVER_URL + "rating/name1_name2"
  POST_DATA_URL = SERVER_URL + "data/name1"

  def is_json(self, resp):
    try:
      json.loads(resp)
      return True
    except ValueError:
      return False 
  
  def test_post_data(self):
    # spotify api from javascript code returns the following string stored in songs
    songs = "{\"0HZk0QsXPhMNAWNDR3rYE8\":{\"danceability\":0.364,\"energy\":0.378,\"key\":7,\"loudness\":-7.381,\"mode\":0,\"speechiness\":0.0331,\"acousticness\":0.47,\"instrumentalness\":0,\"liveness\":0.339,\"valence\":0.366,\"tempo\":89.918,\"type\":\"audio_features\",\"id\":\"0HZk0QsXPhMNAWNDR3rYE8\",\"uri\":\"spotify:track:0HZk0QsXPhMNAWNDR3rYE8\",\"track_href\":\"https://api.spotify.com/v1/tracks/0HZk0QsXPhMNAWNDR3rYE8\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/0HZk0QsXPhMNAWNDR3rYE8\",\"duration_ms\":268960,\"time_signature\":3},\"712uvW1Vezq8WpQi38v2L9\":{\"danceability\":0.587,\"energy\":0.698,\"key\":1,\"loudness\":-8.948,\"mode\":1,\"speechiness\":0.0943,\"acousticness\":0.0152,\"instrumentalness\":0,\"liveness\":0.442,\"valence\":0.0985,\"tempo\":133.954,\"type\":\"audio_features\",\"id\":\"712uvW1Vezq8WpQi38v2L9\",\"uri\":\"spotify:track:712uvW1Vezq8WpQi38v2L9\",\"track_href\":\"https://api.spotify.com/v1/tracks/712uvW1Vezq8WpQi38v2L9\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/712uvW1Vezq8WpQi38v2L9\",\"duration_ms\":310720,\"time_signature\":4},\"0AOvNRgl0SMfOibWA5bP8o\":{\"danceability\":0.716,\"energy\":0.531,\"key\":7,\"loudness\":-7.355,\"mode\":1,\"speechiness\":0.122,\"acousticness\":0.0703,\"instrumentalness\":0,\"liveness\":0.224,\"valence\":0.344,\"tempo\":71.994,\"type\":\"audio_features\",\"id\":\"0AOvNRgl0SMfOibWA5bP8o\",\"uri\":\"spotify:track:0AOvNRgl0SMfOibWA5bP8o\",\"track_href\":\"https://api.spotify.com/v1/tracks/0AOvNRgl0SMfOibWA5bP8o\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/0AOvNRgl0SMfOibWA5bP8o\",\"duration_ms\":386907,\"time_signature\":4},\"0d0jsoba88SpOoFxCxA2rZ\":{\"danceability\":0.555,\"energy\":0.611,\"key\":10,\"loudness\":-9.216,\"mode\":0,\"speechiness\":0.356,\"acousticness\":0.286,\"instrumentalness\":0.00106,\"liveness\":0.273,\"valence\":0.411,\"tempo\":94.939,\"type\":\"audio_features\",\"id\":\"0d0jsoba88SpOoFxCxA2rZ\",\"uri\":\"spotify:track:0d0jsoba88SpOoFxCxA2rZ\",\"track_href\":\"https://api.spotify.com/v1/tracks/0d0jsoba88SpOoFxCxA2rZ\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/0d0jsoba88SpOoFxCxA2rZ\",\"duration_ms\":324533,\"time_signature\":4},\"2P3SLxeQHPqh8qKB6gtJY2\":{\"danceability\":0.779,\"energy\":0.572,\"key\":6,\"loudness\":-9.985,\"mode\":0,\"speechiness\":0.378,\"acousticness\":0.157,\"instrumentalness\":0,\"liveness\":0.246,\"valence\":0.647,\"tempo\":135.956,\"type\":\"audio_features\",\"id\":\"2P3SLxeQHPqh8qKB6gtJY2\",\"uri\":\"spotify:track:2P3SLxeQHPqh8qKB6gtJY2\",\"track_href\":\"https://api.spotify.com/v1/tracks/2P3SLxeQHPqh8qKB6gtJY2\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/2P3SLxeQHPqh8qKB6gtJY2\",\"duration_ms\":300160,\"time_signature\":4},\"2cDCojn6uIBM6A5xTAbl3H\":{\"danceability\":0.487,\"energy\":0.729,\"key\":2,\"loudness\":-6.815,\"mode\":1,\"speechiness\":0.271,\"acousticness\":0.0538,\"instrumentalness\":0.00000407,\"liveness\":0.44,\"valence\":0.217,\"tempo\":91.048,\"type\":\"audio_features\",\"id\":\"2cDCojn6uIBM6A5xTAbl3H\",\"uri\":\"spotify:track:2cDCojn6uIBM6A5xTAbl3H\",\"track_href\":\"https://api.spotify.com/v1/tracks/2cDCojn6uIBM6A5xTAbl3H\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/2cDCojn6uIBM6A5xTAbl3H\",\"duration_ms\":350120,\"time_signature\":4},\"3TBiDIgowqySAT34uJ01nf\":{\"danceability\":0.696,\"energy\":0.485,\"key\":6,\"loudness\":-6.539,\"mode\":1,\"speechiness\":0.051,\"acousticness\":0.0576,\"instrumentalness\":0,\"liveness\":0.183,\"valence\":0.518,\"tempo\":142.891,\"type\":\"audio_features\",\"id\":\"3TBiDIgowqySAT34uJ01nf\",\"uri\":\"spotify:track:3TBiDIgowqySAT34uJ01nf\",\"track_href\":\"https://api.spotify.com/v1/tracks/3TBiDIgowqySAT34uJ01nf\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/3TBiDIgowqySAT34uJ01nf\",\"duration_ms\":127267,\"time_signature\":4},\"5vuJuBqwzHJgCA1ysRfwxZ\":{\"danceability\":0.451,\"energy\":0.831,\"key\":5,\"loudness\":-8.641,\"mode\":0,\"speechiness\":0.307,\"acousticness\":0.0623,\"instrumentalness\":0,\"liveness\":0.185,\"valence\":0.41,\"tempo\":176.162,\"type\":\"audio_features\",\"id\":\"5vuJuBqwzHJgCA1ysRfwxZ\",\"uri\":\"spotify:track:5vuJuBqwzHJgCA1ysRfwxZ\",\"track_href\":\"https://api.spotify.com/v1/tracks/5vuJuBqwzHJgCA1ysRfwxZ\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/5vuJuBqwzHJgCA1ysRfwxZ\",\"duration_ms\":214120,\"time_signature\":4},\"7GZ4WYixf0D6DWvvi60yTM\":{\"danceability\":0.302,\"energy\":0.377,\"key\":3,\"loudness\":-7.606,\"mode\":0,\"speechiness\":0.0889,\"acousticness\":0.143,\"instrumentalness\":0,\"liveness\":0.104,\"valence\":0.237,\"tempo\":83.215,\"type\":\"audio_features\",\"id\":\"7GZ4WYixf0D6DWvvi60yTM\",\"uri\":\"spotify:track:7GZ4WYixf0D6DWvvi60yTM\",\"track_href\":\"https://api.spotify.com/v1/tracks/7GZ4WYixf0D6DWvvi60yTM\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/7GZ4WYixf0D6DWvvi60yTM\",\"duration_ms\":148160,\"time_signature\":5},\"5ujh1I7NZH5agbwf7Hp8Hc\":{\"danceability\":0.716,\"energy\":0.485,\"key\":1,\"loudness\":-7.745,\"mode\":1,\"speechiness\":0.404,\"acousticness\":0.123,\"instrumentalness\":0.0000269,\"liveness\":0.604,\"valence\":0.26,\"tempo\":74.132,\"type\":\"audio_features\",\"id\":\"5ujh1I7NZH5agbwf7Hp8Hc\",\"uri\":\"spotify:track:5ujh1I7NZH5agbwf7Hp8Hc\",\"track_href\":\"https://api.spotify.com/v1/tracks/5ujh1I7NZH5agbwf7Hp8Hc\",\"analysis_url\":\"https://api.spotify.com/v1/audio-analysis/5ujh1I7NZH5agbwf7Hp8Hc\",\"duration_ms\":313787,\"time_signature\":4}}"
    r = requests.post(self.POST_DATA_URL, data = songs)
    self.assertTrue(self.is_json(r.content.decode()))
    resp = json.loads(r.content.decode())
    self.assertEqual(resp['result'], 'success')

  def test_get_rating(self):
    r = requests.get(self.GET_RATING_URL)
    self.assertTrue(self.is_json(r.content.decode()))
    resp = json.loads(r.content.decode())
    print(resp)
    self.assertEqual(resp['rating'], '61')

if (__name__ == "__main__"):
  unittest.main()