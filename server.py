import cherrypy
from spotifyController import SpotifyController

def start_service():
  dispatcher = cherrypy.dispatch.RoutesDispatcher()

  spotifyController = SpotifyController()

  # handle endpoints
  dispatcher.connect('get_rating', '/rating/:name1_name2', controller=spotifyController, action = 'GET_RATING', conditions=dict(method=['GET']))
  dispatcher.connect('post_data', '/data/:name', controller=spotifyController, action = 'POST_DATA', conditions=dict(method=['POST']))

  # default OPTIONS handler for CORS, all direct to the same place
  dispatcher.connect('dict_options', '/dictionary/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
  dispatcher.connect('dict_key_options', '/dictionary/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))


  conf = {
	  'global' : {
			'server.thread_pool': 5,
			'server.socket_host' : 'localhost',
			'server.socket_port': 5000
		},
	'/': {
		'request.dispatch' : dispatcher,
		'tools.CORS.on' : True, # configuration for CORS
		}
	}

  cherrypy.config.update(conf)
  app = cherrypy.tree.mount(None, config=conf)
  cherrypy.quickstart(app)

# class for CORS
class optionsController:
	def OPTIONS(self, *args, **kwargs):
		return ""

# function for CORS
def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # CORS
	start_service()